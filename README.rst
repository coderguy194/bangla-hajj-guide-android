Created by `Md. Abdul Munim Dibosh` using *reStructuredText*

===============================
 Bangla Hajj Guide for Android
===============================

Introduction
============
A simple yet useful tool for people willing to go for Hajj each year. The app is promised to run on any low-end device no matter it has Bangla support or not.
**Current status**: Not updated that much. Incomplete.

Dependency
===========
For universal Bangla support the project uses AndroidCustomFontSupportLib_.

Open Source
===========
This is an Open Source project. You are welcome to contribute. If you are interested please shoot a `mail`_ with **Contribution:Bangla Hajj Guide** as subject line.

.. GENERAL LINKS

.. _`AndroidCustomFontSupportLib`: https://bitbucket.org/coderguy194/androidcustomfontsupportlib/overview
.. _`mail`: abdulmunim.buet@gmail.com