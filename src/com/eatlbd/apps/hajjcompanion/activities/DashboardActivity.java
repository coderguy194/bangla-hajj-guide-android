package com.eatlbd.apps.hajjcompanion.activities;

import android.content.Intent;
import android.os.Bundle;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import android.view.View;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.eatlbd.apps.hajjcompanion.R;
import com.eatlbd.apps.hajjcompanion.customcomponents.DashboardItem;
import com.eatlbd.apps.hajjcompanion.utils.Constants;
import com.eatlbd.apps.hajjcompanion.utils.DashboardItemListener;

public class DashboardActivity extends SherlockFragmentActivity implements DashboardItemListener{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);
        
        ActionBar actionBar=getSupportActionBar();
        actionBar.setTitle(AndroidCustomFontSupport.getStringRepresentationInCustomFont(this, getString(R.string.app_name), Constants.getFont(this, Constants.SEGOE_LIGHT_FONT), (float) 1.5));
        DashboardItem item_essentials=(DashboardItem)findViewById(R.id.dashboard_item_essentials);
        DashboardItem item_types=(DashboardItem)findViewById(R.id.dashboard_item_types);
        DashboardItem item_mustdo=(DashboardItem)findViewById(R.id.dashboard_item_mustdo);
        DashboardItem item_dua=(DashboardItem)findViewById(R.id.dashboard_item_dua);
        DashboardItem item_details=(DashboardItem)findViewById(R.id.dashboard_item_details);
        DashboardItem item_dontdos=(DashboardItem)findViewById(R.id.dashboard_item_dontdos);
        
        item_essentials.setOnItemClickListener(this);
        item_types.setOnItemClickListener(this);
        item_mustdo.setOnItemClickListener(this);
        item_dua.setOnItemClickListener(this);
        item_details.setOnItemClickListener(this);
        item_dontdos.setOnItemClickListener(this);
    }
    
    private void gotoEssentialsActivity() {
		Intent i=new Intent(getApplicationContext(),EssentialsActivity.class);
		startActivity(i);
	}
    
    private void gotoTempTypesActivity()
    {
    	String text=getString(R.string.hajj_ifrad)+getString(R.string.hajj_keran)+getString(R.string.hajj_tamattu);
    	Intent i=new Intent(this,DetailViewActivity.class);
		i.putExtra("text", text);
		startActivity(i);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater=getSupportMenuInflater();
    	inflater.inflate(R.menu.actionbar_items, menu);
    	return true;
    }
	@Override
	public void onItemClick(View v) {
		int tag=Integer.parseInt((String)v.getTag());
		Constants.log(this, "Tag:"+tag);
		switch(tag)
		{
			case 0:
				gotoTempTypesActivity();
				break;
			case 1:
				gotoEssentialsActivity();
				break;
			case 2:
				showNotImplementedMsg();
				break;
			case 3:
				showNotImplementedMsg();
				break;
			case 4:
				showNotImplementedMsg();
				break;
			case 5:
				showNotImplementedMsg();
				break;
		}
	}

	private void showNotImplementedMsg() {
		Intent intent=new Intent(this,DialogActivity.class);
		startActivity(intent);
	}

    
}
