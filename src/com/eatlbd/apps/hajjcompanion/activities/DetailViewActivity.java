package com.eatlbd.apps.hajjcompanion.activities;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.eatlbd.apps.hajjcompanion.R;
import com.eatlbd.apps.hajjcompanion.R.layout;
import com.eatlbd.apps.hajjcompanion.R.menu;
import com.eatlbd.apps.hajjcompanion.fragments.DetailViewFragment;
import com.eatlbd.apps.hajjcompanion.utils.Constants;
import com.eatlbd.apps.hajjcompanion.utils.CustomWebviewClient;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.widget.Toast;

public class DetailViewActivity extends SherlockFragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_view);
        
        ActionBar actionBar=getSupportActionBar();
        actionBar.setTitle(AndroidCustomFontSupport.getStringRepresentationInCustomFont(this, getString(R.string.app_name), Constants.getFont(this, Constants.SEGOE_LIGHT_FONT), (float) 1.5));
//        actionBar.setTitle(Constants.getSpannableWithSize(this,, Constants.SEGOE_LIGHT_FONT,1.5f));
        actionBar.setDisplayHomeAsUpEnabled(true);
        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        
        String text=getIntent().getStringExtra("text");
        
        DetailViewFragment fragment=new DetailViewFragment(text);
        fragmentTransaction.add(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
        
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater=getSupportMenuInflater();
    	inflater.inflate(R.menu.actionbar_items, menu);
    	return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch(item.getItemId())
    	{
    		case android.R.id.home:
    			// app icon in action bar clicked; go home
                Intent intent = new Intent(this, DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
    		default:
                return super.onOptionsItemSelected(item);
    		
    	}
    }
}
