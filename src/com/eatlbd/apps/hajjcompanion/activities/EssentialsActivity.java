package com.eatlbd.apps.hajjcompanion.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.eatlbd.apps.hajjcompanion.R;
import com.eatlbd.apps.hajjcompanion.utils.Constants;
import com.eatlbd.apps.hajjcompanion.utils.FragmentSwiper;


public class EssentialsActivity extends SherlockFragmentActivity {

	ActionBar actionBar;
	ViewPager viewPager;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_essentials);
        actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        viewPager=(ViewPager)findViewById(R.id.pager);
        
        FragmentManager fm=getSupportFragmentManager();
        
        ViewPager.SimpleOnPageChangeListener pageChangeListener= new ViewPager.SimpleOnPageChangeListener()
        {
        	@Override
        	public void onPageSelected(int pos)
        	{
        		super.onPageSelected(pos);
        		actionBar.setSelectedNavigationItem(pos);
        	}
        };
        
        viewPager.setOnPageChangeListener(pageChangeListener);
        
        FragmentSwiper fragmentSwiper= new FragmentSwiper(fm);
        
        viewPager.setAdapter(fragmentSwiper);
        
        actionBar.setDisplayShowTitleEnabled(true);
        
        actionBar.setTitle(AndroidCustomFontSupport.getStringRepresentationInCustomFont(this, getString(R.string.app_name), Constants.getFont(this, Constants.SEGOE_LIGHT_FONT), (float) 1.5));
        
        ActionBar.TabListener tabListener=new ActionBar.TabListener() {
			
			@Override
			public void onTabUnselected(Tab tab, FragmentTransaction ft) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onTabSelected(Tab tab, FragmentTransaction ft) {
				viewPager.setCurrentItem(tab.getPosition());
			}
			
			@Override
			public void onTabReselected(Tab tab, FragmentTransaction ft) {
				// TODO Auto-generated method stub
				
			}
		};
		
		
		
		Tab tab= actionBar.newTab();
		tab.setText(AndroidCustomFontSupport.getStringRepresentationInCustomFont(this, getString(R.string.app_name), Constants.getFont(this, Constants.SEGOE_LIGHT_FONT), (float) 1.5));
//		tab.setText(AndroidCustomFontSupport.getCorrectedBengaliFormatForNativeComponents(getString(R.string.e_mustknow_tab),new Typeface[]{Constants.getFont(this, Constants.SOLAIMAN_LIPI),Constants.getFont(this, Constants.KARNAFULI_BIJOY_FONT)},-1));
		
		tab.setText(AndroidCustomFontSupport.getCorrectedBengaliFormat(getString(R.string.e_mustknow_tab),Constants.getFont(this, Constants.SOLAIMAN_LIPI),1.5f));
		tab.setTabListener(tabListener);
		
		actionBar.addTab(tab);
		
		
		tab= actionBar.newTab();
		tab.setText(AndroidCustomFontSupport.getCorrectedBengaliFormat(getString(R.string.e_number_tab),Constants.getFont(this, Constants.SOLAIMAN_LIPI),1.5f));
		tab.setTabListener(tabListener);
		
		actionBar.addTab(tab);
        
    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getSupportMenuInflater().inflate(R.menu.actionbar_items, menu);
//        return true;
//    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater=getSupportMenuInflater();
    	inflater.inflate(R.menu.actionbar_items, menu);
    	return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch(item.getItemId())
    	{
    		case android.R.id.home:
    			// app icon in action bar clicked; go home
                Intent intent = new Intent(this, DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
    		default:
                return super.onOptionsItemSelected(item);
    		
    	}
    }

    
}
