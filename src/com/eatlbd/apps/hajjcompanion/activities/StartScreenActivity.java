package com.eatlbd.apps.hajjcompanion.activities;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.eatlbd.apps.hajjcompanion.R;
import com.eatlbd.apps.hajjcompanion.utils.Constants;

public class StartScreenActivity extends FragmentActivity {

    Timer waitman=new Timer();
    Activity context;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        
        TextView title=(TextView)findViewById(R.id.lblTitle);
        title.setTypeface(Constants.getFont(this, Constants.SEGOE_LIGHT_FONT));
        
        TextView motto=(TextView)findViewById(R.id.lblMotto);
        motto.setTypeface(Constants.getFont(this, Constants.SEGOE_LIGHT_FONT));
        
        context=this;
    }
    @Override
	public void onStart() {
		super.onStart();
		waitman.schedule(new TimerTask() {			
			@Override
			public void run() {
				
				Intent intent = new Intent(context, DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
			}
			
		}, 3000);
	}
    
}
