package com.eatlbd.apps.hajjcompanion.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import com.eatlbd.apps.hajjcompanion.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ContactListAdapter extends BaseAdapter{

	Activity mContext;
	ArrayList<HashMap<String,String>> items= new ArrayList<HashMap<String,String>>();
	public ContactListAdapter(Activity ctx)
	{
		this.mContext=ctx;
	}
	public void setItems(ArrayList<HashMap<String,String>> items)
	{
		this.items.clear();
		this.items.addAll(items);
		notifyDataSetChanged();
	}
	public void addItem(HashMap<String,String> item)
	{
		items.add(item);
		notifyDataSetChanged();
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}

	@Override
	public HashMap<String, String> getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView == null) {
			convertView = mContext.getLayoutInflater().inflate(R.layout.contact_info, null, false);
			holder = new ViewHolder();
			holder.lblName = (TextView) convertView.findViewById(R.id.lblName);
			holder.lblNumber=(TextView)convertView.findViewById(R.id.lblNumber);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		@SuppressWarnings("unchecked")
		HashMap<String,String> content = getItem(position);
		holder.lblName.setText(content.get("name"));
		holder.lblNumber.setText(content.get("number"));
		
		
		return convertView;
	}
	static class ViewHolder {
		public TextView lblName;
		public TextView lblNumber;
	}

}
