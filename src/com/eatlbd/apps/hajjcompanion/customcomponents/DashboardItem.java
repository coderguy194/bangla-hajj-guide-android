package com.eatlbd.apps.hajjcompanion.customcomponents;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.eatlbd.apps.hajjcompanion.R;
import com.eatlbd.apps.hajjcompanion.utils.Constants;
import com.eatlbd.apps.hajjcompanion.utils.DashboardItemListener;

public class DashboardItem extends LinearLayout implements OnClickListener{

	DashboardItemListener onItemClickListener;
	public DashboardItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater.from(context).inflate(R.layout.dashboard_item, this, true);
		RelativeLayout icon_container=(RelativeLayout)findViewById(R.id.icon_container);
		ImageView icon=(ImageView)findViewById(R.id.icon_image);
		TextView text=(TextView)findViewById(R.id.text);
		TypedArray a = context.obtainStyledAttributes(attrs,
			    R.styleable.dashboard_item);
			 
		final int N = a.getIndexCount();
		for (int i = 0; i < N; i++)
		{
		    int attr = a.getIndex(i);
		    switch (attr)
		    {
		        case R.styleable.dashboard_item_text:
		            String myText = a.getString(attr);
//		            setBanglaUsingLib(context, text, myText);
		            text.setText(AndroidCustomFontSupport.getCorrectedBengaliFormat(myText,Constants.getFont(context,Constants.SOLAIMAN_LIPI), -1));
		            break;
		        case R.styleable.dashboard_item_icon_image:
		            Drawable icon_image = a.getDrawable(attr);
		            icon.setImageDrawable(icon_image);
		            break;
		    }
		}
		a.recycle();
		//
		icon_container.setOnClickListener(this);//this is a bubbling of event.
		//as child relative layout captures the click,we transfer it to parent.

	}
	public void setOnItemClickListener(DashboardItemListener listener)
	{
		this.onItemClickListener=listener;
	}
	private void setBanglaTypeface(TextView text,Context context,String fontname)
	{
		if (!text.isInEditMode()) {
            text.setTypeface(Constants.getFont(context, fontname));
		}
	}
	@Override
	public void onClick(View v) {
		if(this.onItemClickListener != null)
		{
			this.onItemClickListener.onItemClick(this);//we should pass this,as we set tag for 
			//view root
		}
	}

}
