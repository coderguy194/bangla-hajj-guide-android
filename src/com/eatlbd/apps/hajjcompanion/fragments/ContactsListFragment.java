package com.eatlbd.apps.hajjcompanion.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.eatlbd.apps.hajjcompanion.R;
import com.eatlbd.apps.hajjcompanion.adapters.ContactListAdapter;

public class ContactsListFragment extends SherlockListFragment {

	ContactListAdapter adapter;
	@Override
	public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState)
	{
		adapter=new ContactListAdapter(getActivity());
		setListAdapter(adapter);
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	@Override
	public void onStart()
	{
		super.onStart();
		try {
			readContacts();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				String number=adapter.getItem(pos).get("number");
				String uri="tel:"+number.trim();
				Intent intent=new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse(uri));
				getActivity().startActivity(intent);
			}
		});
		
	}

	
	private void readContacts() throws Exception
	{
		InputStream input= getActivity().getAssets().open("files/hajj_agents.txt");
		BufferedReader reader=new BufferedReader(new InputStreamReader(input));
		String line="";
		ArrayList<HashMap<String,String>> items=new ArrayList<HashMap<String,String>>();
		while((line=reader.readLine())!=null)
		{
			StringTokenizer tokens = new StringTokenizer(line,"\t");
			String name=tokens.nextToken();
			String number=tokens.nextToken();
			HashMap<String, String> item = new HashMap<String, String>();
			item.put("name", name);
			item.put("number", number);
			items.add(item);
		}
		this.adapter.setItems(items);
	}
}
