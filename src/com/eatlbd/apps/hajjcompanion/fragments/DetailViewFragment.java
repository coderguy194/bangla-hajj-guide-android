package com.eatlbd.apps.hajjcompanion.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.actionbarsherlock.app.SherlockFragment;
import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.eatlbd.apps.hajjcompanion.R;
import com.eatlbd.apps.hajjcompanion.adapters.ContactListAdapter;
import com.eatlbd.apps.hajjcompanion.utils.Constants;
import com.eatlbd.apps.hajjcompanion.utils.CustomWebviewClient;
import com.eatlbd.apps.hajjcompanion.utils.UIInformer;

@SuppressLint("ValidFragment")
public class DetailViewFragment extends SherlockFragment implements UIInformer{
	
	WebView myBrowser;
	CustomWebviewClient client;
	String text= "";
	ProgressDialog loading;
	@SuppressLint("ValidFragment")
	public DetailViewFragment(String text)
	{
//		this.text=text;
		//webviews need utf-8 strings
		this.text=AndroidCustomFontSupport.getCorrectedBengaliFormatAsString(text);
	}
	public void reload()
	{
		loading.show();
		this.myBrowser.reload();
	}
	@Override
	public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_detail, container, false);
        
		return view;
	}
	private void initComponents() {
		myBrowser=(WebView)getActivity().findViewById(R.id.browser);
		
//		myBrowser.getSettings().enableSmoothTransition();
		myBrowser.getSettings().setSupportZoom(true);
		myBrowser.getSettings().setBuiltInZoomControls(true);
		
		this.client=new CustomWebviewClient(getActivity(), this);
        
		myBrowser.setWebViewClient(this.client);
        
        loading = new ProgressDialog(getActivity());
		loading.setCancelable(false);
		loading.setCanceledOnTouchOutside(false);
		loading.setMessage(AndroidCustomFontSupport.getCorrectedBengaliFormat(getString(R.string.please_wait),Constants.getFont(getActivity(), Constants.SOLAIMAN_LIPI), -1));
	}
	public void setWebviewClient(CustomWebviewClient client)
	{
		this.client=client;
		this.myBrowser.setWebViewClient(this.client);
	}
	private void showDetailsView()
	{
		
		Display display = getActivity().getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels/2;
        int margin=5;
        
        String head = "<head><style>@font-face {font-family: 'myface';src: url('fonts/solaimanlipinormal.ttf');} body {font-family: 'myface'; background-image:url('images/bg.png');} img { width: " +(width-2*margin) + "px; height: "+(height-2*margin)+"px;}</style></head>";
        
        String htmlData = "<html>" + head + "<body>" + text + "</body></html>";
        myBrowser.loadDataWithBaseURL("file:///android_asset/", htmlData, "text/html", "UTF-8", null);
	}
	@Override
	public void onStart()
	{
		super.onStart();
		initComponents();
		try {
			loading.show();
			showDetailsView();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Override
	public void sendMessage(String msg) {
		if(msg.equals(Constants.WEBVIEW_LOADED))
		{
			loading.dismiss();
		}
	}

}
