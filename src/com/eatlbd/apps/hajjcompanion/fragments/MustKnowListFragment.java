package com.eatlbd.apps.hajjcompanion.fragments;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.eatlbd.apps.hajjcompanion.R;
import com.eatlbd.apps.hajjcompanion.utils.Constants;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.eatlbd.apps.hajjcompanion.activities.DetailViewActivity;
import com.eatlbd.apps.hajjcompanion.adapters.ContactListAdapter;

public class MustKnowListFragment extends SherlockListFragment{

	SimpleBanglaListAdapter adapter;
	@Override
	public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState)
	{
		adapter=new SimpleBanglaListAdapter(getActivity(),getActivity().getResources().getStringArray(R.array.must_knows_titles),Constants.getFont(getActivity(), Constants.SOLAIMAN_LIPI));
		setListAdapter(adapter);
		
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	private void gotoDetails(int pos)
	{
		String text="";
		switch(pos)
		{
			case 0:
				text=getActivity().getString(R.string.tawaf);
				break;
			case 1:
				text=getActivity().getString(R.string.sayee);
				break;
			case 2:
				text=getActivity().getString(R.string.taalbia);
				break;
			case 3:
				text=getActivity().getString(R.string.daam);
				break;
			
		}
		Intent i=new Intent(getActivity(),DetailViewActivity.class);
		i.putExtra("text", text);
		getActivity().startActivity(i);
		
	}
	@Override
	public void onStart()
	{
		super.onStart();
		getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				
					gotoDetails(pos);
			}
		});
		
	}
	
	/**
	 * Show list item of single string with Bangla font.
	 * @author Md. Abdul Munim Dibosh
	 *
	 */
	class SimpleBanglaListAdapter extends BaseAdapter
	{

		ArrayList<String> items=new ArrayList<String>();
		Activity mContext;
		Typeface typeFace;//typeface for all the list items
		public SimpleBanglaListAdapter(Activity ctx,String[] array, Typeface tf)
		{
			this.mContext=ctx;
			for(String str:array)
			{
				this.items.add(str);
			}
			this.typeFace=tf;
		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return items.size();
		}

		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			String text =AndroidCustomFontSupport.getCorrectedBengaliFormatAsString(items.get(position));
			return text;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			TextView tv=null;
			if(convertView== null)
			{
				convertView=mContext.getLayoutInflater().inflate(R.layout.must_know_list_item, null,false);
				tv=(TextView) convertView.findViewById(R.id.topic_title);
				tv.setTypeface(typeFace);
				convertView.setTag(tv);
			}
			else
			{
				tv=(TextView) convertView.getTag();
			}
			tv.setText(this.getItem(position));
			return convertView;
		}
		
		
		
	}
}
