package com.eatlbd.apps.hajjcompanion.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import androidbangladesh.bengali.support.BengaliUnicodeString;

public class Constants {
	
	public static String SUTONNI_MJ="sutonni.TTF";
	public static String SOLAIMAN_LIPI="solaimanlipinormal.ttf";
	public static String TITLE_FONT="titlefont.ttf";
	public static String SEGOE_LIGHT_FONT="segoeuil.ttf";
	public static String KARNAFULI_BIJOY_FONT="karnafully.ttf";
	
	public static String WEBVIEW_LOADED="loaded";

	/**
	 * Logs messages based on the class it gets sent from
	 * @param obj
	 * @param msg
	 */
	public static void log(Object obj,String msg)
	{
		Log.d(obj.getClass().getSimpleName(), msg);
	}
	
	public static Typeface getFont(Context context,String fontName)
	{
		return Typeface.createFromAsset(context.getAssets(), "fonts/"+fontName);
	}

}
