package com.eatlbd.apps.hajjcompanion.utils;

import android.app.Activity;
import android.content.Context;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class CustomWebviewClient extends WebViewClient{
	
	Context context;
	UIInformer delegate;
	public CustomWebviewClient(Context ctx,UIInformer delegate)
	{
		this.context=ctx;
		this.delegate=delegate;
	}
	@Override
	public void onPageFinished(WebView view, String url) {
		// TODO Auto-generated method stub
		super.onPageFinished(view, url);
		this.delegate.sendMessage(Constants.WEBVIEW_LOADED);
	}
	@Override
    public boolean shouldOverrideUrlLoading(WebView view, String url)
    {
        if(url.contains("duas"))
        {
        	
        }
        return true;
    }

}
