package com.eatlbd.apps.hajjcompanion.utils;

import android.view.View;

public interface DashboardItemListener{
	
	public void onItemClick(View v);

}
