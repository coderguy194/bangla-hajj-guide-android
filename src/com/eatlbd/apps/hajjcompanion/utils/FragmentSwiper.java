package com.eatlbd.apps.hajjcompanion.utils;

import com.eatlbd.apps.hajjcompanion.fragments.ContactsListFragment;
import com.eatlbd.apps.hajjcompanion.fragments.MustKnowListFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class FragmentSwiper extends FragmentPagerAdapter{

	final int PAGE_COUNT=2;
	public FragmentSwiper(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		Bundle data=new Bundle();
		switch(position)
		{
			//Contacts tab
			case 1:
				ContactsListFragment contactsFragment= new ContactsListFragment();
				data.putInt("current_page", position+1);
				contactsFragment.setArguments(data);
				return contactsFragment;
			//Contacts tab
			case 0:
				MustKnowListFragment mustKnowFragment= new MustKnowListFragment();
				data.putInt("current_page", position+1);
				mustKnowFragment.setArguments(data);
				return mustKnowFragment;
		
		}
		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return PAGE_COUNT;
	}

}
