package com.eatlbd.apps.hajjcompanion.utils;
/**
 * 
 * @author Md. Abdul Munim Dibosh
 * Used to inform UI changes from one view to another.
 */
public interface UIInformer {

	public void sendMessage(String msg);
}
